--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Control.Applicative
import           Control.Monad
import           Control.Monad.Extra
import           Data.Maybe
import qualified Data.Text as T
import           Data.Time
import           Hakyll
import           Text.Pandoc (ReaderOptions, Pandoc, WriterOptions, runPure, writerSlideLevel)
import           Text.Pandoc.Writers.HTML (writeRevealJs)
import           System.FilePath


--------------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {
  destinationDirectory = "public"
}

main :: IO ()
main = hakyllWith config $ do
  match "images/*" $ do
    route   idRoute
    compile copyFileCompiler

  match "css/*" $ do
    route   idRoute
    compile compressCssCompiler

  match "*.org" $ do
    route   $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/default.html" (defaultContext <> nextUpcomingCtx)
      >>= relativizeUrls

  match "*.html" $ do
    route   idRoute
    compile compressCssCompiler


  match "talks/*" $ do
    route $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/talk.html"    (talkCtx <> slidesCtx)
      >>= loadAndApplyTemplate "templates/default.html" talkCtx
      >>= relativizeUrls

  match "talks/*" $ version "slides" $ do
    route $ customRoute slidesPath
    compile $ pandocCompileSlides defaultHakyllReaderOptions (defaultHakyllWriterOptions {writerSlideLevel = Just 2})
      >>= loadAndApplyTemplate "templates/presentation.html" defaultContext
      >>= relativizeUrls

  create ["talks.html"] $ do
    route idRoute
    compile $ do
      talks <- recentFirst =<< allTalks
      (upcoming, past) <- partitionM isUpcoming talks
      let talksCtx =
            listField "talks"    talkCtx (return past)     <>
            listField "upcoming" talkCtx (return upcoming) <>
            constField "title" "Talks"                     <>
            defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/talks.html" talksCtx
        >>= loadAndApplyTemplate "templates/default.html" talksCtx
        >>= relativizeUrls

  create ["atom.xml"] $ do
    route idRoute
    compile $ do
      talks <- fmap (take 10) . recentFirst =<< allTalks
      renderAtom myFeedConfiguration talkCtx talks

  create ["rss.xml"] $ do
    route idRoute
    compile $ do
      talks <- fmap (take 10) . recentFirst =<< allTalks
      renderRss myFeedConfiguration talkCtx talks


  match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
allTalks = loadAll ("talks/*" .&&. hasNoVersion)

talkCtx :: Context String
talkCtx =
  escapeTitle <>
  dateField "date" "%B %e, %Y" <>
  defaultContext


-- from https://github.com/jaspervdj/hakyll/issues/706#issuecomment-581557187
escapeTitle :: Context String
escapeTitle = Context $ \key -> case key of
                                  "title" -> unContext (mapContext escapeHtml defaultContext) key
                                  _       -> unContext mempty key

headTalk :: Compiler [Item String] -> Compiler String
headTalk c = maybe empty talkAlert . listToMaybe =<< c
  where talkAlert i = itemBody <$> loadAndApplyTemplate "templates/next.html" talkCtx i

nextUpcomingCtx :: Context String
nextUpcomingCtx = field "next" $ const $ headTalk $ chronological =<< filterM isUpcoming =<< allTalks

slidesPath =  (<> "-slides.html") . dropExtension . toFilePath

slidesCtx :: Context String
slidesCtx = field "slides" $ return . slidesPath . itemIdentifier

isUpcoming = isUpcoming' . itemIdentifier

isUpcoming' :: Identifier -> Compiler Bool
isUpcoming' i = do
  now <- unsafeCompiler $ getCurrentTime
  idDate <- getItemUTC defaultTimeLocale i
  return $ utctDay idDate >= utctDay now

-- From https://github.com/nigelsim
writePandocSlideWith :: WriterOptions -> Item Pandoc -> Item String
writePandocSlideWith wopt (Item itemi doc) =
    case runPure $ writeRevealJs wopt doc of
        Left err    -> error $ "writePandocSlidesWith: " ++ show err
        Right item' -> Item itemi $ T.unpack item'

pandocCompileSlides :: ReaderOptions -> WriterOptions -> Compiler (Item String)
pandocCompileSlides ropt wopt =
    cached "pandocCompileSlides" $
        writePandocSlideWith wopt <$>
        (traverse (return.id) =<< readPandocWith ropt =<< getResourceBody)
--

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
  { feedTitle       = "M-x Erlangen"
  , feedDescription = "Talks of the M-x Erlangen meetup"
  , feedAuthorName  = "Merlin Göttlinger"
  , feedAuthorEmail = "megoettlinger+mxerlangen@gmail.com"
  , feedRoot        = "https://m-x-erlangen.gitlab.io"
  }
