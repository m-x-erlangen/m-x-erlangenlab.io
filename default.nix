let
  sources = import ./nix/sources.nix {};
in
{ nixpkgs ? import sources.nixpkgs {}, compiler ? "ghc865" }:

{
  site = nixpkgs.haskellPackages.callPackage ./site.nix {};
}
