---
title: New Features in Emacs 27&28
date: 2020-06-24
location: DFNconf
time: 18:30
description: We will learn about Philip's experiences playing around with Emacs 28 and talk about the new features introduced in 27.
conf-link: https://conf.dfn.de/webapp/conference/979117107
author: Philip
---
