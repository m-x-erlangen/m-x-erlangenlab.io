---
title: Lightning Talks - Org-Roam and Leaf
date: 2020-05-27
location: DFNconf
time: 17:45
description: Lightning talks
conf-link: https://conf.dfn.de/webapp/conference/979117107
---
Note that the time is earlier for this meeting to avoid overlap with the [[https://www.meetup.com/de-DE/Regensburg-Haskell-Meetup/events/270767122/][Haskell meetup on the same day]].

For our May meeting, we plan to have multiple short presentations from different
speakers on individual topics. So far, our schedule is as follows:
- Hans-Peter will talk about org-roam
- Merlin will talk about leaf

This schedule is by no means complete. So if you have some topic to add, feel
free to drop us a line and we'll add it to the schedule.
