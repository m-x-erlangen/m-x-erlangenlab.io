---
title: A Conversation with Org -- Document, Develop and Configure with Org-Babel
author: Jan
date: 2022-03-01
location: online
conf-link: https://fau.zoom.us/j/68647308541?pwd=a3VoK3ZzQkFseGNwSmdZczFiamtNQT09
time: 18:30
description: Learning about code execution in org-buffers
manual-slides: https://gitlab.com/m-x-erlangen/talks/-/tree/master/intro-org-babel
---
In our March meeting, Jan will give an introductory talk on how to
use org-babel for documentation, configuration and literate
programming within your org-buffers.
