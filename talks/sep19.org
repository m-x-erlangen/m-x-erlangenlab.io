---
title: Introduction to org-ref
author: Melanie
date: 2019-09-23
location: Cauerstraße 11
room: 00.131
time: 18:30
description: An introduction to the all famous org-ref
manual-slides: https://gitlab.com/m-x-erlangen/talks/tree/master/intro_org_ref
---
In our September meeting, Mel will give an introductory talk about how to manage
bibliography, citations, and cross-references with org-ref.
