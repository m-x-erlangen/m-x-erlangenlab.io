---
title: Doom Emacs -- introduction and first impression
author: Christoph
date: 2021-03-29
location: DFNconf
time: 18:30
description: Doom Emacs is an opinionated Emacs distribution/framework designed to be fast, close to vanilla, and strives to follow the Nix/Guix philosophy.
conf-link: https://conf.dfn.de/webapp/conference/979117107
---

I recently switched from Spacemacs to an Emacs distribution that is closer to
vanilla, but still offers a lot of abstraction making it easier to use and
configure: Doom Emacs. I will talk about my first impressions after the switch,
explain what makes Doom great and how it works.

* Why an Emacs distribution?
** coming from =vim=, I wanted certain things like =evil-mode= out of the box
** the Emacs ecosystem has
- a huge list of packages
- multiple packages for almost any task, e.g.
  + =helm= vs =ivy= vs =ido= (completion frameworks)
  + package managers: =use-package=, =straight=, =el-get=, =borg=, …
** therefore, an opinionated collection cannot hurt!
** *major* evilified distributions to choose from:
  + Spacemacs
  + Doom Emacs
* Why I went the Spacemacs way first
** Spacemacs has the larger community
Doom is basically just a one-man show
** It is also "more complete"
A lot more packages bundled as so-called *layers*
** Doom advertises itself as being *closer to metal*
… and I wanted as little fuss as possible
* What went wrong?
** Complicated processes for customization
- A feature is not already in Spacemacs?
- "The best way would be to create your own layer!" is what I read often
- Creating layers is way too much overhead
** Complicated config file
- the default Spacemacs config is already 500 lines
- spread over multiple functions, =user-config=, =user-init=, =user-load=
- variables of packages in =layers= are configured in another function
- hard to keep the file coherent
** I often stumbled over =customize-variable=
- Spacemacs saves the customized stuff in the =.spacemacs= file
- It often happened to me that things appeared there that I didn't want
** Rolling release often broke my Emacs
** Spacemacs is slow.
- Feature-completeness also means a lot is loaded by default
- The startup time is not really optimized
- It often feels sluggish and it's hard to poinpoint the problem without going
  too deep into Emacs debugging
* Why Doom makes Emacs great again
** Enter Doom Emacs
- A configuration framework for Emacs
- conceived and maintained by [[https://github.com/hlissner][Henrik Lissner]]
- runs its own package management system on top of =straight=
- comes with a standard library to simplify configuration
** What is Doom's philosophy?
- "Gotta go fast."
- "Close to metal."
- "Opinionated, but not stubborn."
- "Your system, your rules."
- "Nix/Guix is a great idea!"
** Gotta go fast
- Doom is blazingly fast
- the startup time can compete with =vim=
- but how?
  + avoids gc at startup
  + specialized mechanism to concatenate autoloads
  + lazy load package management
  + lazy load stuff when Emacs is idle
  + some more technical stuff
** Close to metal
- much less abstraction in the config files
- well-documented set of additional functions (see demo later)
** Opinionated, but not stubborn
- it's actually easy to override defaults, just like in vanilla
- =customize= system is turned off!
- builtin mechanism to trigger initializing code with a variant of =setq=
** Your system, your rules
- Spacemacs sometimes compiles system dependencies
- Doom is designed not to, and to force plugins not to either
- =doom doctor= will tell you what's missing
** Nix/Guix is a great idea!
- Doom comes with a binary (actually a shell script running an Emacs lisp program), =doom=
- =doom sync= is comparable to =nixos-rebuild switch= for those familiar with =nix=
- reads the config files
- downloads all the packages, which are *pinned* (no rolling release!)
- generates the *actual* Emacs configuration
* Demo
* First impression (after a few weeks)
** I'm never going back
- Doom strikes the perfect balance for me
- Arguably better defaults than Spacemacs
- "module" set (pre-configured packages) not as large as Spacemacs, but sufficient
** Minor quirks/issues
- A few keybindings are not very "spacemacy" (but that's easy to fix!)
- The main support channel is via Discord, which might put off some people
- Don't expect bugs to be fixed quickly, or documentation to be complete
