---
title: Virtual Hot Punch Meeting
author: Melanie
date: 2020-12-16
location: DFNconf
conf-link: https://conf.dfn.de/webapp/conference/979117107
time: 18:30
description: Virtual bring-your-own punch meeting with ad-hoc talks.
---
We'll meet virtually and enjoy some hot punch (or whatever liquid you prefer).

As of now, we do not have any planned talk yet. Therefore, if you have anything
in mind worth sharing, please let us know. Alternatively, we can have ad-hoc
(mini-)talks.
